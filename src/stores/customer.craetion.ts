import { defineStore } from 'pinia';

export const useCreatedCustomerStore = defineStore({
  id: 'customerStore',
  state: () => ({
    id: null,
  }),
  actions: {
    setCustomerId(id) {
      this.id = id;
    },
  },
});
