import { defineStore } from 'pinia';

export const useManagerStore = defineStore({
    id: 'manager',
    state: () => ({
        currentCustomerManagerId: null as string,
        currentCustomerManagerName: null as string,
    }),
    actions: {
        setCurrentCustomerManagerId(managerId: string) {
            this.currentCustomerManagerId = managerId;
        },
        setCurrentCustomerManagerName(managerName: string) {
            this.currentCustomerManagerName = managerName;
        },
    },
});
