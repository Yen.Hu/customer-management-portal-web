import { defineStore } from 'pinia';

function getCookie(name: string): string | undefined {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop()?.split(';').shift();
}

export const useSessionStore = defineStore({
  id: 'session',
  state: () => ({
    isJSessionIDSet: getCookie('JSESSIONID') !== undefined,
  }),
  actions: {
    updateIsJSessionIDSet() {
      this.isJSessionIDSet = getCookie('JSESSIONID') !== undefined;
    }
  },
});