import { createRouter, createWebHistory } from 'vue-router';
import LoginComponent from '../components/LoginComponent.vue';
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: LoginComponent
    },
    {
      path: '/customer/portal',
      name: 'customer-portal',
      component: () => import('../components/CustomerPortal.vue')
    },
    {
      path: '/customer/create',
      name: 'customer',
      component: () => import('../components/CustomerCreation.vue')
    },
    {
      path: '/customer/edit/:id',
      name: 'customer-edit',
      component: () => import('../components/CustomerEdit.vue'),
      props: true
    },
    {
      path: '/customer/show',
      name: 'customer-information',
      component: () => import('../components/CustomerInformation.vue')
    }
  ]
})

export default router
